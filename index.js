const menu_icon = document.querySelector(".hamburgermenu");
const nav_bar = document.querySelector(".navbar");
const nav_item_click = document.querySelector(".test_link");

nav_item_click.addEventListener("click", () => {
  nav_bar.classList.toggle("change");
});

menu_icon.addEventListener("click", () => {
  nav_bar.classList.toggle("change");
});
